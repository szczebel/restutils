package restcall;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.List;
import java.util.function.Supplier;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class RestCall<T> {
    final Class<T> expectedClass;
    final MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
    final HttpHeaders headers;
    private final String url;
    Object body;
    private Supplier<? extends RestTemplate> templateSupplier = RestTemplate::new;

    public RestCall(String url, Class<T> expectedClass) {
        this.url = url;
        this.expectedClass = expectedClass;
        this.headers = new HttpHeaders();
        headers.setAccept(singletonList(APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    }

    public static <T> RestGet<T> restGet(String url, Class<T> expectedClass) {
        return new RestGet<>(url, expectedClass);
    }

    public static <T> RestGetList<T> restGetList(String url, Class<T[]> expectedClass) {
        return new RestGetList<>(url, expectedClass);
    }

    public static <T> RestPost<T> restPost(String url, Class<T> expectedClass) {
        return new RestPost<>(url, expectedClass);
    }

    public static RestPostVoid restPostVoid(String url) {
        return new RestPostVoid(url);
    }

    public static RestDelete restDelete(String url) {
        return new RestDelete(url);
    }

    public RestCall<T> httpBasic(String username, String password) {
        String plainClientCredentials = String.join(":", username, password);
        String base64ClientCredentials = Base64.getEncoder().encodeToString((plainClientCredentials.getBytes()));
        headers.add("Authorization", "Basic " + base64ClientCredentials);
        return this;
    }

    public RestCall<T> authToken(String token) {
        headers.add("Authorization", "Bearer " + token);
        return this;
    }

    public RestCall<T> restTemplate(Supplier<? extends RestTemplate> factory) {
        this.templateSupplier = factory;
        return this;
    }

    public RestCall<T> param(String key, Object value) {
        params.add(key, value);
        return this;
    }

    public RestCall<T> body(Object body) {
        this.body = body;
        return this;
    }

    final String originalUrl() {
        return url;
    }

    String url() {
        String retval = originalUrl();
        if (!params.isEmpty()) retval += "?";
        for (String key : params.keySet()) {
            List<Object> values = params.get(key);
            for (Object value : values) {
                retval += key + "=" + String.valueOf(value) + "&";
            }
        }
        return retval;
    }

    RestTemplate template() {
        return templateSupplier.get();
    }

    public HttpEntity<?> req() {
        return new HttpEntity<>(headers);
    }

    public RestCall<T> contentType(MediaType type) {
        headers.setContentType(type);
        return this;
    }

    public T execute() {
        throw new UnsupportedOperationException("Override to provide implementation");
    }

    public static abstract class RestParametrizedCall<T> extends RestCall<T> {

        RestParametrizedCall(String url, Class<T> expectedClass) {
            super(url, expectedClass);
        }

        public HttpEntity<?> req() {
            boolean isJson = headers.getContentType().equals(APPLICATION_JSON) || headers.getContentType().equals(APPLICATION_JSON_UTF8);
            HttpEntity<Object> e = new HttpEntity<>(isJson ? body : params, headers);
            //todo: log request body in DEBUG
            return e;
        }

        @Override
        String url() {
            return originalUrl();
        }
    }

    public static class RestGetList<T> extends RestCall<T[]> {

        private RestGetList(String url, Class<T[]> expectedClass) {
            super(url, expectedClass);
        }

        @Override
        public T[] execute() {
            return template().exchange(
                    url(),
                    HttpMethod.GET,
                    req(),
                    expectedClass
            ).getBody();
        }
    }

    public static class RestGet<T> extends RestCall<T> {

        private RestGet(String url, Class<T> expectedClass) {
            super(url, expectedClass);
        }

        public T execute() {
            return template().exchange(url(), HttpMethod.GET, req(), expectedClass).getBody();
        }

    }

    public static class RestPost<T> extends RestParametrizedCall<T> {

        private final String url;
        private final Class<T> expectedClass;

        private RestPost(String url, Class<T> expectedClass) {
            super(url, expectedClass);
            this.url = url;
            this.expectedClass = expectedClass;
        }

        public T execute() {
            return template().postForObject(url, req(), expectedClass);
        }

    }

    public static class RestDelete extends RestCall<Void> {

        private RestDelete(String url) {
            super(url, Void.class);
        }

        public Void execute() {
            template().exchange(url(), HttpMethod.DELETE, req(), Void.class);
            return null;
        }
    }

    public static class RestPostVoid extends RestParametrizedCall<Void> {

        private RestPostVoid(String url) {
            super(url, Void.class);
        }

        public Void execute() {
            template().postForEntity(url(), req(), Void.class);
            return null;
        }

    }
}
