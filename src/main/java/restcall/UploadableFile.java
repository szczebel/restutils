package restcall;

import org.springframework.core.io.ByteArrayResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@SuppressWarnings("unused")
public class UploadableFile extends ByteArrayResource {

    public static UploadableFile forUpload(File file) throws IOException {
        return new UploadableFile(file.getName(), Files.readAllBytes(file.toPath()));
    }

    private final String fileName;

    private UploadableFile(String fileName, byte[] fileBytes) {
        super(fileBytes);
        this.fileName = fileName;
    }

    @Override
    public String getFilename() {
        return fileName;
    }
}
