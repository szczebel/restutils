package restcall;

import org.springframework.lang.NonNull;

import java.util.Objects;

public class PlaceholderString {

    private final String template;

    public static PlaceholderString placeholder(@NonNull String template) {
        return new PlaceholderString(template);
    }

    public PlaceholderString(@NonNull String template) {
        this.template = template;
    }

    public String fill(Object... actuals) {
        String retval = template;
        for (Object actual : actuals) {
            int start = retval.indexOf('{');
            if(start == -1) throw new IllegalArgumentException("No place to insert " + actual + " missing {");
            int end = retval.indexOf('}');
            if(end == -1) throw new IllegalArgumentException("No place to insert " + actual + " missing }");
            if(end<start) throw new IllegalArgumentException("No place to insert " + actual + ", }...{");
            retval = retval.substring(0, start) + actual + retval.substring(end + 1);
        }

        int start = retval.indexOf('{');
        int end = retval.indexOf("}");
        if(start!=-1 && end>start) throw new IllegalArgumentException("Not enough actuals");

        return retval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlaceholderString)) return false;
        PlaceholderString that = (PlaceholderString) o;
        return Objects.equals(template, that.template);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(template);
    }

    @Override
    public String toString() {
        return template;
    }

    public static void main(String[] args) {
        System.out.println(
                placeholder("http://{host}:{port}/api/entity/{id}/action")
                .fill("yahoo.com", 8080, 5L)
        );
    }
}
